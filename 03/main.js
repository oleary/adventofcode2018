const lineByLine = require('n-readlines');
const liner = new lineByLine('./day03-input.txt');
const rect = require('./lib/rect');

let line;
let lineNumber = 0;
let fileLines = []
let dev = true;

while (line = liner.next()) {
    //console.log('Line ' + lineNumber + ': ' + line.toString('ascii'));
    lineNumber++;
    fileLines.push(line.toString('ascii'))
}

lines = fileLines;

if (dev) {
    let lines = [
        '#1 @ 1,3: 4x4',
        '#2 @ 3,1: 4x4',
        '#3 @ 5,5: 2x2',
        '#4 @ 5,5: 1x1'
    ]
}

let maxx = 0
let maxy = 0

//console.log(rect.readInput(lines[1], false));
//console.log(parseRect(rect.readInput(lines[1], false)))
/*
let box1 = rect.readInput(lines[3], false)
let box2 = rect.readInput(lines[2], false)

console.log(
    rect.intersects(box1, box2)
)
*/

//MAXX - max(x+w).  MAXY - max(y+h)

for (l = 0; l < lines.length; l++) {

    let box = rect.readInput(lines[l]);
    let boxx = box.x + box.w;
    let boxy = box.y + box.h;

    if (boxx > maxx) { maxx = boxx }
    if (boxy > maxy) { maxy = boxy }
}

let currx = 0
let curry = 0
let intersections = 0

while (currx < maxx || curry < maxy) {
    for (b = 0; b < lines.length; b++) {
        let box = rect.readInput(lines[b]);
        let smb = rect.readInput(lines[l]);
    }

    currx++
}

console.log(maxx);
console.log(maxy)