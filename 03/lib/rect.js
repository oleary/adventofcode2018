const insect = require('intersects');

function intersects(box1, box2) {
    return insect.boxBox(
        box1.x,
        box1.y,
        box1.w,
        box1.h,
        box2.x,
        box2.y,
        box2.w,
        box2.h
    )
}

function readInput(line, id) {
    //#2 @ 3,1: 4x4
    let ret = { }

    let spl1 = line.split(" @ ") // .split(",").split(":").split("x")
    let spl2 = spl1[1].split(",")
    let spl3 = spl2[1].split(": ")
    let spl4 = spl3[1].split("x")

    if (id) {
        ret.id = parseInt(spl1[0].toString().replace("#", ""))
    }
    
    ret.x = parseInt(spl2[0])
    ret.y = parseInt(spl3[0])
    ret.w = parseInt(spl4[0])
    ret.h = parseInt(spl4[1])

    return ret
}

module.exports.readInput = readInput;
module.exports.intersects = intersects;