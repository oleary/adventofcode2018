package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func readInput(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func main() {
	lines, err := readInput("../input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	ans := 0
	var s []int

	for {
		for _, line := range lines {
			j, err := strconv.Atoi(line)
			if err != nil {
				log.Fatalf("strinconv: %s", err)
			}
			ans += j
			if contains(s, ans) {
				fmt.Println("WHOO00: ", ans)
				os.Exit(0)
			}
			s = append(s, ans)
			//fmt.Println(i, line, j, ans)
		}
	}

	fmt.Println("output: ", ans)

	printSlice(s)

}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
