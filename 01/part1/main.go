package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func readInput(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func main() {
	lines, err := readInput("../input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	ans := 0

	for i, line := range lines {
		//fmt.Println(i, line)
		j, err := strconv.Atoi(line)
		if err != nil {
			log.Fatalf("strinconv: %s", err)
		}
		fmt.Println(i, line, j)
		ans += j
	}

	fmt.Println("output: ", ans)

}
