var fs = require("fs");
var data = fs.readFileSync('./input.txt');

runit(data.toString());

function runit(events) {
    const sortedEvents = [];
  
    for (const ev of events.split('\n')) {
      const [ , dt, type, , id] = ev.match(/\[(.*)\]\s(\w+)\s(.{1})(\d+|\w+)/);
      sortedEvents.push([dt, type, id]);
    }

    sortedEvents.sort(sortFunction);

    let gaurdSleeps = [];

    for (i = 0; i < sortedEvents.length; i++) {
        if (sortedEvents[i][1] == "Guard") {
            if (sortedEvents[i+1][1] == 'falls') {
                const [ , sleep] = sortedEvents[i+1][0].match(/:(\d+)/);
                const [ , wake] = sortedEvents[i+2][0].match(/:(\d+)/);
                gaurdSleeps.push( {
                    id: sortedEvents[i][2],
                    sleep: sleep,
                    wake: wake })
            }
        } else {
            continue;
        }
    }


    let sleepTimes = gaurdSleeps.map(function (g) {
        let t = parseInt(g.wake) - parseInt(g.sleep);
        return {id: g.id, sleepT: t, sleep: g.sleep, wake: g.wake}
    })

    let sleepH = gaurdSleeps.map(function (g) {
        let ret = { id: g.id }
        for (i = 0; i < 61; i++) {
            ret[i] = 0
        }
        return ret
    })

    for (j = 0; j < sleepH.length; j++) {
        let thisId = sleepH[j].id;
        for (n = 0; n < sleepTimes.length; n++) {
            if (sleepTimes[n].id == thisId) {
                let start = sleepTimes[n].sleep;
                let end = sleepTimes[n].wake;
                for (m = 0; m < 61; m++) {
                    if (m > start && m < end) {
                        sleepH[j][m.toString()]++
                    }
                }
            }
        }
    }

    let topId;
    let topMin = 0;
    let topMinX = 0;

    for (d = 0; d < sleepH.length; d++) {
        let thisId = sleepH[d].id;
        for (f = 0; f < 61; f++) {
            if (sleepH[d][f.toString()] > topMinX) {
                topid = sleepH[d].id;
                topMin = f;
                topMinX = sleepH[d][f.toString()];
            }
        }
    }

    console.log(topid, topMin, topMinX);

    /*
    var result = [];
    sleepTimes.reduce(function (res, value) {
        if (!res[value.id]) {
            res[value.id] = {
                sleepT: 0,
                id: value.id
            };
            result.push(res[value.id])
        }
        res[value.id].sleepT += value.sleepT
        return res;
    }, {});

    console.log(result)
*/
/*
    gaurdSleeps.sort();
    let currGaurd = ''
    let currGSleepTotal = 0;
    let gTotals = [];

    for (j = 0; j < gaurdSleeps.length; j++) {
        //console.log(gaurdSleeps[j][0])
        if (currGaurd == gaurdSleeps[j][0]) {
            currGSleepTotal += (parseInt(gaurdSleeps[j][2]) - parseInt(gaurdSleeps[j][1]))
        } else {
            gTotals.push([currGaurd, currGSleepTotal])
            currGSleepTotal = 0;
            currGaurd = gaurdSleeps[j][0];
        }
    }

    let bigg = gTotals.sort(sortFunction1).reverse()[0][0]
    console.log (bigg);
    let mins = [];

    for (n = 0; n < 61; n++) {
        for (u = 0; u < gaurdSleeps.length; u++) {
            
        }
    } 
    console.log("")
*/
    /*
    for (u = 0; u < gaurdSleeps.length; u++) {
        if (bigg == gaurdSleeps[u][0]) {
            let start = gaurdSleeps[u][1];
            let end = gaurdSleeps[u][2]
            console.log(start, end)
            for (m = 0; m < 61; m++) {
                if (m > start && m < end) {
                    mins[m]++
                }
            }
        }
    }

    for (h = 0; h < mins.length; h++) {
        console.log(":" + h, mins[h])
    }
    */

  };
  

function sortFunction(a, b) {
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] < b[0]) ? -1 : 1;
    }
}

function sortFunction1(a, b) {
    if (a[1] === b[1]) {
        return 0;
    }
    else {
        return (a[1] < b[1]) ? -1 : 1;
    }
}