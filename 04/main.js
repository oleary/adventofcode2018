
var fs = require("fs");
var data = fs.readFileSync('./input.txt');

runit(data.toString());

function runit(events) {
    const sortedEvents = [];
  
    for (const ev of events.split('\n')) {
      const [ , dt, type, , id] = ev.match(/\[(.*)\]\s(\w+)\s(.{1})(\d+|\w+)/);
      //console.log([dt, type, id]);
      sortedEvents.push([dt, type, id]);
    }

    sortedEvents.sort(sortFunction);
    //console.log(sortedEvents);

    let gaurdSleeps = [];

    for (i = 0; i < sortedEvents.length; i++) {
        if (sortedEvents[i][1] == "Guard") {
            if (sortedEvents[i+1][1] == 'falls') {
                const [ , sleep] = sortedEvents[i+1][0].match(/:(\d+)/);
                const [ , wake] = sortedEvents[i+2][0].match(/:(\d+)/);
                gaurdSleeps.push([sortedEvents[i][2], sleep, wake])
            }
        } else {
            continue;
        }
    }


    gaurdSleeps.sort();
    let currGaurd = ''
    let currGSleepTotal = 0;
    let gTotals = [];

    for (j = 0; j < gaurdSleeps.length; j++) {
        //console.log(gaurdSleeps[j][0])
        if (currGaurd == gaurdSleeps[j][0]) {
            currGSleepTotal += (parseInt(gaurdSleeps[j][2]) - parseInt(gaurdSleeps[j][1]))
        } else {
            gTotals.push([currGaurd, currGSleepTotal])
            currGSleepTotal = 0;
            currGaurd = gaurdSleeps[j][0];
        }
    }

    let bigg = gTotals.sort(sortFunction1).reverse()[0][0]
    console.log (bigg);
    let mins = [];

    for (n = 0; n < 61; n++) {
        mins[n] = 0
    } 
    console.log("")
    
    for (u = 0; u < gaurdSleeps.length; u++) {
        if (bigg == gaurdSleeps[u][0]) {
            let start = gaurdSleeps[u][1];
            let end = gaurdSleeps[u][2]
            console.log(start, end)
            for (m = 0; m < 61; m++) {
                if (m > start && m < end) {
                    mins[m]++
                }
            }
        }
    }

    for (h = 0; h < mins.length; h++) {
        console.log(":" + h, mins[h])
    }

  };
  

function sortFunction(a, b) {
    if (a[0] === b[0]) {
        return 0;
    }
    else {
        return (a[0] < b[0]) ? -1 : 1;
    }
}

function sortFunction1(a, b) {
    if (a[1] === b[1]) {
        return 0;
    }
    else {
        return (a[1] < b[1]) ? -1 : 1;
    }
}