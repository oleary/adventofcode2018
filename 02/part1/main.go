package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func readInput(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func main() {
	lines, err := readInput("../day02-input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}

	var twice, thrice int = 0, 0

	for _, line := range lines {
		//numberOfa := strings.Count(line, "a")
		//fmt.Println(i, line, "numberOfa", numberOfa)
		//fmt.Println(line)
		thisM := make(map[byte]int)

		for j := 0; j < len(line); j++ {
			thisB := line[j]
			//fmt.Printf("%x ", thisB)
			thisM[thisB]++
		}
		//fmt.Println("trucks")
		//fmt.Println(thisM)
		if mapkey(thisM, 2) {
			twice++
		}
		if mapkey(thisM, 3) {
			thrice++
		}
	}

	fmt.Println("donzo")
	fmt.Println("Twice: ", twice)
	fmt.Println("Thrice: ", thrice)
	fmt.Println("CHECKSUM: ", twice*thrice)

}

func mapkey(m map[byte]int, value int) (ok bool) {
	for _, v := range m {
		if v == value {
			ok = true
			return
		}
	}
	return
}
