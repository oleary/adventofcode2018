const lineByLine = require('n-readlines');
const liner = new lineByLine('../day02-input.txt');

let line;
let lineNumber = 0;
let fileLines = []
let dev = false;

while (line = liner.next()) {
    //console.log('Line ' + lineNumber + ': ' + line.toString('ascii'));
    lineNumber++;
    fileLines.push(line.toString('ascii'))
}

console.log('end of line reached');


let lines = [
    'abcde',
    'fghij',
    'klmno',
    'pqrst',
    'fguij',
    'axcye',
    'wvxyz'
]

if (!(dev)){ 
    lines = fileLines
}


for (i = 0; i < lines.length; i++) { 
    if (dev) { console.log("Starting ", lines[i]); }

    for (s = 0; s < lines.length; s++) {
        if (s == i) { continue; }
        if (dev) { console.log("   Compared to: ", lines[s]); }
        mismatch = 0
        match = 0

        for (j = 0; j < lines[i].length; j++) {
            if (lines[i].charAt(j) == lines[s].charAt(j)) {
                match++;
            } else {
                mismatch++;
            }
        }

        if (dev) { console.log("     Matches: ", match); }
        if (dev) { console.log("     Mismatches: ", mismatch); }

        if (mismatch < 2) {
            console.log(" WOOOOOO ")
            console.log(lines[i]);
            console.log(lines[s]);
            console.log(" WOOOOOO ")
        }

        /*
        xretqmmonskvzupalfiwhcfdb
        xretqmmonskvzupalfiwhcfdb
        */
    }

}